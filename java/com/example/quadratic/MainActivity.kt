package com.example.quadratic

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

@Suppress("PLUGIN_WARNING")
class MainActivity : AppCompatActivity() {

    private fun calculateQuadratic(a: Double, b: Double, c: Double):String {
        val discriminant = b*b - 4*a*c
        var result: String
        if (discriminant > 0) {
            result = ((-b + Math.sqrt(discriminant)) / 2 * a).toString()
            result += " , " + ((-b - Math.sqrt(discriminant)) / 2 * a).toString()
        }
        else if (discriminant == 0.0) {
            result = ((-b) / 2 * a).toString()
        }
        else {
            result = "discriminant < 0 (("
        }
        if (result == "-0.0") {
            result = "0.0"
        }
        return result
    }

    fun backToNormal(view: View) {
        quedrificationResult.visibility = View.INVISIBLE
        button2.visibility = View.INVISIBLE
        button2.isClickable = false
    }

    fun outputResult(view: View) {
        button2.visibility = View.VISIBLE
        button2.isClickable = true
        quedrificationResult.visibility = View.VISIBLE
        var a = 0.0
        var b = 0.0
        var c = 0.0
        var isCorrectInput = true
        try {
            a = editText.text.toString().toDouble()
            b = editText2.text.toString().toDouble()
            c = editText4.text.toString().toDouble()
        }
        catch (e: NumberFormatException) {
            quedrificationResult.text = "NumberFormatException((("
            isCorrectInput = false
        }
        if ((a == 0.0) && (isCorrectInput)) {
            quedrificationResult.text =  "'a' coefficient cant be 0"
        }
        else if ((a != 0.0) && (isCorrectInput)) {
            quedrificationResult.text = calculateQuadratic(a, b, c)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
}
